package com.kvmnet.aplicacion20fragmentcorreo.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kvmnet.aplicacion20fragmentcorreo.R;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.Sitio;

import java.util.ArrayList;

public class AdaptadorCustomObject extends ArrayAdapter<Sitio> {

    Activity context;
    ArrayList<Sitio> datos;

    public AdaptadorCustomObject(Fragment context, ArrayList<Sitio> datos) {
        super(context.getActivity(), R.layout.layout_element, datos);
        this.context = context.getActivity();
        this.datos = datos;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.layout_element, null);

        ImageView imageView = item.findViewById(R.id.imageView);
        imageView.setImageResource(datos.get(position).getImg());

        TextView lblNombre =  item.findViewById(R.id.lbl_nombre);
        lblNombre.setText(datos.get(position).getNombre());

        TextView lblDesc =  item.findViewById(R.id.lbl_desc);
        lblDesc.setText(datos.get(position).getDescr());


        return item;
    }
}
