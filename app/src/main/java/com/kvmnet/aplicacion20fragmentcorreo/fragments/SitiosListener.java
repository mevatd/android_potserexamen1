package com.kvmnet.aplicacion20fragmentcorreo.fragments;

import com.kvmnet.aplicacion20fragmentcorreo.pojo.Sitio;

public interface SitiosListener {
    void onSitioSeleccionado(Sitio c);
}
