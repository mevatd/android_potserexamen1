package com.kvmnet.aplicacion20fragmentcorreo.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.kvmnet.aplicacion20fragmentcorreo.R;
import com.kvmnet.aplicacion20fragmentcorreo.activities.Activity_ListaSitios;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.Sitio;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.TipoSitio;

import java.util.ArrayList;

public class Fragment_IntroducirSitios extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    ArrayList<TipoSitio> tipos = new ArrayList<>();
    ArrayList<String> tiposString = new ArrayList<>();

    EditText txtNombre;
    EditText txtDesc;

    RadioGroup rGrp;
    Spinner spnrTipo;
    Button btnAdd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_introducir_sitios, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    /*    Intent intent = getActivity().getIntent();
        Bundle bundle = intent.getExtras();
        tipos =  (ArrayList<TipoSitio>) bundle.getSerializable("tipos");
*/

        tipos.add(new TipoSitio(R.drawable.beach, "Playa"));
        tipos.add(new TipoSitio(R.drawable.city, "Ciudad"));
        tipos.add(new TipoSitio (R.drawable.mountain, "Montaña"));
        tipos.add(new TipoSitio (R.drawable.restaurant, "Restaurante"));

        txtNombre = getActivity().findViewById(R.id.txt_nombre);
        txtDesc = getActivity().findViewById(R.id.txt_desc);

        rGrp = getActivity().findViewById(R.id.rg_nivel);

        for (int i = 0; i <tipos.size(); i++){
            tiposString.add(tipos.get(i).getTipo());
        }

        spnrTipo = getActivity().findViewById(R.id.spnr_tipo);
        spnrTipo.setAdapter(new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, tiposString));
        spnrTipo.setOnItemSelectedListener(this);

        btnAdd = getActivity().findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (!tiposString.get(position).equals("Restaurante")){

            rGrp.setVisibility(View.GONE);
        }else{
            rGrp.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onClick(View v) {
        Sitio s = new Sitio(tipos.size()+1, tipos.get(spnrTipo.getSelectedItemPosition()).getImg(), "sadsad", "asdasd", "asdas","asdas");
        Bundle bundle = new Bundle();
        bundle.putSerializable("sitio", s);
        Intent intent = new Intent(getContext(), Activity_ListaSitios.class);
        intent.putExtras(bundle);
        startActivity(intent);
        getActivity().finish();
    }
}
