package com.kvmnet.aplicacion20fragmentcorreo.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kvmnet.aplicacion20fragmentcorreo.R;
import com.kvmnet.aplicacion20fragmentcorreo.adapter.AdaptadorCustomObject;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.Sitio;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.TipoSitio;

import java.util.ArrayList;

public class Fragment_ListaSitios extends Fragment implements AdapterView.OnItemClickListener {

    private ArrayList<Sitio> datos = new ArrayList<>();
    private ArrayList<TipoSitio> tipos = new ArrayList<>();
    private ListView lstListado;

    SitiosListener listener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_fragment_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

       /* trozo de codigo para recoger el sitio añadido desde la otra actividad
        Intent intent = getActivity().getIntent();
        Bundle bundle = intent.getExtras();

        Sitio ss = (Sitio) bundle.getSerializable("sitio");
        */
        //initialize types
        tipos.add(new TipoSitio(R.drawable.beach, "Playa"));
        tipos.add(new TipoSitio(R.drawable.city, "Ciudad"));
        tipos.add(new TipoSitio (R.drawable.mountain, "Montaña"));
        tipos.add(new TipoSitio (R.drawable.restaurant, "Restaurante"));

        lstListado = getView().findViewById(R.id.LstListado);


        datos.add(new Sitio(0, tipos.get(0).getImg(), "Miramar", "De abuelos", tipos.get(0).getTipo()));
        datos.add(new Sitio(1, tipos.get(1).getImg(), "Cuenca", "Interior de España", tipos.get(1).getTipo()));
        datos.add(new Sitio(2, tipos.get(2).getImg(), "Castillo de Xativa", "VVVVoniko", tipos.get(2).getTipo()));
        datos.add(new Sitio(3, tipos.get(3).getImg(), "El tunel de tapas", "Muy TOP", tipos.get(3).getTipo(), "caro"));

        lstListado.setAdapter(new AdaptadorCustomObject(this, datos));


        lstListado.setOnItemClickListener(this);

    }

    public void setSitiosListener(SitiosListener listener) {
        this.listener = listener;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (listener != null) {
            listener.onSitioSeleccionado((Sitio) lstListado.getAdapter().getItem(position));
        }
    }
}
