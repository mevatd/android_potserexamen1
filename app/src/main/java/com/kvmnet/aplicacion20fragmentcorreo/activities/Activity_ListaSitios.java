package com.kvmnet.aplicacion20fragmentcorreo.activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kvmnet.aplicacion20fragmentcorreo.R;
import com.kvmnet.aplicacion20fragmentcorreo.dialogs.DetailDialog;
import com.kvmnet.aplicacion20fragmentcorreo.fragments.Fragment_ListaSitios;
import com.kvmnet.aplicacion20fragmentcorreo.fragments.SitiosListener;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.Sitio;

public class Activity_ListaSitios extends AppCompatActivity implements SitiosListener {

    Fragment_ListaSitios frgListado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_lista_sitios);

        frgListado = (Fragment_ListaSitios) getSupportFragmentManager().findFragmentById(R.id.FrgListado);
        frgListado.setSitiosListener(this);

        //boolean hayDetalle = (getSupportFragmentManager().findFragmentById(R.id.FrgDetalle) != null);



    }

    @Override
    public void onSitioSeleccionado(Sitio s) {

        FragmentManager fragmentManager = getSupportFragmentManager();
       /* DialogConfirm dialogConfirm = new DialogConfirm();
        dialogConfirm.show(fragmentManager, "tagConfirm");*/



        DetailDialog detailDialog = DetailDialog.newInstance(s);
        detailDialog.show(fragmentManager, "detail");


    }
}
