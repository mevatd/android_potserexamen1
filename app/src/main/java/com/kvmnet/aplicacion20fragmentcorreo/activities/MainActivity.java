package com.kvmnet.aplicacion20fragmentcorreo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kvmnet.aplicacion20fragmentcorreo.R;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.TipoSitio;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnVer;
    Button btnAdd;
    Button btnExit;

    ArrayList<TipoSitio> tipos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnVer = findViewById(R.id.btn_list);
        btnVer.setOnClickListener(this);

        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);

        btnExit = findViewById(R.id.btn_exit);
        btnExit.setOnClickListener(this);

   /*     tipos.add(new TipoSitio(R.drawable.beach, "Playa"));
        tipos.add(new TipoSitio(R.drawable.city, "Ciudad"));
        tipos.add(new TipoSitio (R.drawable.mountain, "Montaña"));
        tipos.add(new TipoSitio (R.drawable.restaurant, "Restaurante"));*/
    }

    @Override
    public void onClick(View v) {

        Intent intent;

        switch (v.getId()) {

            case R.id.btn_list:
                intent = new Intent(this, Activity_ListaSitios.class);
                startActivity(intent);
                break;

            case R.id.btn_add:
         //       Bundle bundle = new Bundle();
            //    bundle.putSerializable("tipos", tipos);
                intent = new Intent(this, Activity_IntroducirSitios.class);
            //    intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.btn_exit:
                finish();
                break;
        }


    }
}
