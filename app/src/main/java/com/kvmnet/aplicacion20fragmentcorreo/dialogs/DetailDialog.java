package com.kvmnet.aplicacion20fragmentcorreo.dialogs;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kvmnet.aplicacion20fragmentcorreo.R;
import com.kvmnet.aplicacion20fragmentcorreo.pojo.Sitio;


public class DetailDialog extends DialogFragment {

    ImageView imgTipo;
    TextView txtNombre;
    TextView txtDesc;
    TextView txtType;

    ImageView imgPrecio;
    TextView lblPrecio;
    TextView txtPrecio;


    Button btnDismiss;


    public static DetailDialog newInstance(Sitio sitio) {

        DetailDialog detailDialog = new DetailDialog();

        Bundle bundle = new Bundle();

        bundle.putSerializable("detail", sitio);

        detailDialog.setArguments(bundle);


        return detailDialog;
    }

    //set our custom layout
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        return inflater.inflate(R.layout.sitio_details_dialog, container);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //get object from bundle
        Sitio s = (Sitio) getArguments().getSerializable("detail");

        //get fields from view to set text
        imgTipo = view.findViewById(R.id.img_type);
        imgTipo.setImageResource(s.getImg());

        txtNombre = view.findViewById(R.id.textView_nombre);
        txtNombre.setText(s.getNombre());

        txtDesc = view.findViewById(R.id.textView_desc);
        txtDesc.setText(s.getDescr());

        txtType = view.findViewById(R.id.textView_type);
        txtType.setText(s.getTipo());

        imgPrecio = view.findViewById(R.id.imageView4);
        lblPrecio = view.findViewById(R.id.lbl_precio);

        txtPrecio = view.findViewById(R.id.textView_precio);
        if(s.getPrecio() == null){
            imgPrecio.setVisibility(View.GONE);
            lblPrecio.setVisibility(View.GONE);
            txtPrecio.setVisibility(View.GONE);
        }else{
            txtPrecio.setText(s.getPrecio());
        }



        btnDismiss = view.findViewById(R.id.button_dismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              getDialog().dismiss();
                                          }
                                      }
        );
    }
}
