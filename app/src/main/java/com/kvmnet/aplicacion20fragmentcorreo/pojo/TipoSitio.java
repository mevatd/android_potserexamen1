package com.kvmnet.aplicacion20fragmentcorreo.pojo;

import java.io.Serializable;

public class TipoSitio implements Serializable {
    private int img;
    private String tipo;

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public TipoSitio(int img, String tipo) {

        this.img = img;
        this.tipo = tipo;
    }
}
