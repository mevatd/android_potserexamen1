package com.kvmnet.aplicacion20fragmentcorreo.pojo;

import java.io.Serializable;

public class Sitio implements Serializable {

    private int id;
    private int img;
    private String nombre;
    private String descr;
    private String tipo;
    private String precio;

    public Sitio(int id, int img, String nombre, String descr, String tipo) {
        this.id = id;
        this.img = img;
        this.nombre = nombre;
        this.descr = descr;
        this.tipo = tipo;
    }

    public Sitio(int id, int img, String nombre, String descr, String tipo, String precio) {
        this.id = id;
        this.img = img;
        this.nombre = nombre;
        this.descr = descr;
        this.tipo = tipo;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
